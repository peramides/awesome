#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run autorandr -c
run setxkbmap -option compose:ralt
run xbindkeys
run mate-power-manager
run nm-applet
run redshift-gtk
run pasystray
run xscreensaver -nosplash
run xss-lock -- xscreensaver-command -lock
#run protonmail-bridge --no-window

